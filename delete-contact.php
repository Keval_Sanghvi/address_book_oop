<?php
require_once 'init.php';

if (isset($_GET['id'])) {
    // Delete contact with this id
    $id = $_GET['id'];

    $rows = $database->table("contacts")->where('id', '=', $id)->get();
    if ($rows === false) {
        $error = db_error();
        die(var_dump($error));
    }
    $image_name = $rows[0]->image_name;
    unlink("images/users/$image_name");
    // Query to delete from DB
    $result = $database->table('contacts')->deleteWhere(['id' => $id]);

    if ($result) {
        header("Location: index.php?q=success&op=delete");
    } else {
        header("Location: index.php?q=error&op=delete");
    }
}
