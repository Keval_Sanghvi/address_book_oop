<?php
require_once 'init.php';
class NewContact
{
    protected Database $database;
    protected string $first_name;
    protected string $last_name;
    protected string $email;
    protected string $telephone;
    protected string $address;
    protected string $birthdate;
    protected string $image_name;

    public function __construct(Database $database, string $first_name, string $last_name, string $email, string $telephone, string $address, string $birthdate, string $image_name)
    {
        $this->database = $database;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->address = $address;
        $this->birthdate = $birthdate;
        $this->image_path = $image_name;
    }

    public function set_first_name(string $first_name): void
    {
        $this->first_name = $first_name;
    }

    public function set_last_name(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    public function set_email(string $email): void
    {
        $this->email = $email;
    }

    public function set_telephone(string $telephone): void
    {
        $this->telephone = $telephone;
    }

    public function set_address(string $address): void
    {
        $this->address = $address;
    }

    public function set_birthdate(string $birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    public function set_image_name(string $image_name): void
    {
        $this->image_name = $image_name;
    }

    public function get_first_name(): string
    {
        return $this->first_name;
    }

    public function get_last_name(): string
    {
        return $this->last_name;
    }

    public function get_email(): string
    {
        return $this->email;
    }

    public function get_telephone(): string
    {
        return $this->telephone;
    }

    public function get_address(): string
    {
        return $this->address;
    }

    public function get_birthdate(): string
    {
        return $this->birthdate;
    }

    public function get_image_name(): string
    {
        return $this->image_name;
    }

    public function insert_in_db(array $data): bool
    {
        $this->database->table("contacts")->insert($data);
        return true;
    }

    public function update_in_db(array $data, int $id): bool
    {
        $this->database->table("contacts")->update($data, $id);
        return true;
    }
}
