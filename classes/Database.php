<?php
class Database
{
    protected string $host;
    protected string $dbName;
    protected string $username;
    protected string $password;
    protected PDO $pdo;
    protected PDOStatement $stmt;
    protected string $table;
    protected bool $debug = true;

    public function __construct()
    {
        $configurations = parse_ini_file('config.ini');
        $this->host = $configurations['host'];
        $this->dbName = $configurations['dbname'];
        $this->username = $configurations['username'];
        $this->password = $configurations['password'];
        $dsn = "mysql:host={$this->host};dbname={$this->dbName}";
        try {
            $this->pdo = new PDO($dsn, $this->username, $this->password);
            if ($this->debug) {
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        } catch (PDOException $e) {
            die($this->debug ? $e->getMessage() : 'Some Database Issue!');
        }
    }

    public function query(string $sql): ?PDOStatement
    {
        return $this->pdo->query($sql);
    }

    public function table(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    public function fetchAll(string $sql): array
    {
        return $this->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function where(string $field, string $operator, string $value): self
    {
        $this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE {$field} {$operator} :value");
        $this->stmt->execute(['value' => $value]);
        return $this;
    }

    public function get(): array
    {
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function count(): int
    {
        return $this->stmt->rowCount();
    }

    public function first(): ?object
    {
        return $this->count() ? $this->get()[0] : null;
    }

    public function exists($data): bool
    {
        $field = array_keys($data)[0];
        return $this->where($field, "=", $data[$field])->count() ? true : false;
    }

    public function deleteWhere($data): bool
    {
        $field = array_keys($data)[0];
        $value = array_values($data)[0];
        $sql = "DELETE FROM {$this->table} WHERE {$field} = :value";
        $this->stmt  = $this->pdo->prepare($sql);
        return $this->stmt->execute(['value' => $value]);
    }

    public function insert($data): bool
    {
        $keys = array_keys($data);
        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholders = ":" . implode(", :", $keys);
        $sql = "INSERT INTO {$this->table}({$fields}) VALUES({$placeholders})";
        $this->stmt = $this->pdo->prepare($sql);
        return $this->stmt->execute($data);
    }

    public function update($data, $id): bool
    {
        $keys = array_keys($data);
        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholders = ":" . implode(", :", $keys);
        $sql = "UPDATE {$this->table} SET";
        foreach ($data as $item => $value) {
            $sql .= " `$item`=:$item,";
        }
        $sql = substr($sql, 0, -1);
        $sql .= " WHERE id=$id";
        $this->stmt = $this->pdo->prepare($sql);
        return $this->stmt->execute($data);
    }
}
